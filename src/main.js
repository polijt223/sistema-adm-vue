import Vue from 'vue'
import Vuex from 'vuex'
//import AXIOS
import axios from 'axios'
import VueAxios from 'vue-axios'
//import decode JWT   
import decode from 'jwt-decode'
import App from './App.vue'
import router from './router'
//IMPORT ICON FONT-AWESOME
import { library } from '@fortawesome/fontawesome-svg-core'
import { faKey,faUserTie,faSave,faBackspace,faBan,faCheckDouble,faEyeSlash,faEye,faHome,
          faPlusCircle,faTrashAlt,faUsers,faPen,faUserSecret,faArrowAltCircleRight,
          faArrowAltCircleLeft,faArrowAltCircleUp,faArrowAltCircleDown,faClipboardList }
          from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
//IMPORT BOOTSTRAP
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


axios.defaults.baseURL='http://localhost:3000/api/'
Vue.use(Vuex)
Vue.use(VueAxios, axios)
Vue.use(BootstrapVue, decode)

library.add(faHome, faPlusCircle,faTrashAlt,faPen,faUsers,faArrowAltCircleUp,faArrowAltCircleRight,
            faEyeSlash,faArrowAltCircleLeft,faArrowAltCircleDown,faClipboardList,faEye,
            faBan,faCheckDouble,faSave,faBackspace,faKey,faUserTie)

Vue.component('font-awesome-icon',FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
