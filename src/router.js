import Vue from 'vue'
import Router from 'vue-router'
import store from './store'
import Home from './views/Home.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/admin/profile',
      name: 'profile',
      component: () => import('./views/Profile.vue'),
      meta: {
        administrador: true,
        almacenero: true,
        vendedor: true,
      }
    },
    {
      path: '/admin/home',
      name: 'home',
      component: Home,
      meta: {
        libre: true
      }
    },
    {
      path: '/admin/articulos',
      name: 'articulos',
      component: () => import('./components/sistemaAdmin/abm/almacen/Articulos.vue'),
      meta: {
        administrador: true,
        almacenero: true,
        vendedor: true,
      }
    },
    {
      path: '/admin/categorias',
      name: 'categorias',
      component: () => import('./components/sistemaAdmin/abm/almacen/Categorias.vue'),
      meta: {
        administrador: true,
        almacenero: true,
        vendedor: true,
      }
    },
    {
      path: '/admin/ingresos',
      name: 'ingresos',
      component: () => import('./components/sistemaAdmin/abm/compras/Ingresos.vue'),
      meta: {
        administrador: true,
        almacenero: true,
        vendedor: true,
      }
    },
    {
      path: '/admin/proveedores',
      name: 'proveedores',
      component: () => import('./components/sistemaAdmin/abm/compras/Proveedores.vue'),
      meta: {
        administrador: true,
        almacenero: true,
        vendedor: true,
      }
    },
    {
      path: '/admin/ventas',
      name: 'ventas',
      component: () => import('./components/sistemaAdmin/abm/ventas/Ventas.vue'),
      meta: {
        administrador: true,
        almacenero: true,
        vendedor: true,
      }
    },
    {
      path: '/admin/clientes',
      name: 'clientes',
      component: () => import('./components/sistemaAdmin/abm/ventas/Clientes.vue'),
      meta: {
        administrador: true,
        almacenero: true,
        vendedor: true,
      }
    },
    {
      path: '/admin/usuarios',
      name: 'usuarios',
      component: () => import('./components/sistemaAdmin/abm/accesos/Usuarios.vue'),
      meta: {
        administrador: true,
      }
    },
    {
      path: '/admin/consulta/compras',
      name: 'consulta/compras',
      component: () => import('./components/sistemaAdmin/abm/consultas/ConsultaCompras.vue'),
      meta: {
        administrador: true,
        almacenero: true,
        vendedor: true,
      }
    },
    {
      path: '/admin/consulta/ventas',
      name: 'consulta/ventas',
      component: () => import('./components/sistemaAdmin/abm/consultas/ConsultaVentas.vue'),
      meta: {
        administrador: true,
        almacenero: true,
        vendedor: true,
      }
    },
    {
      path: '/admin/login',
      name: 'login',
      component: () => import('./components/sistemaAdmin/login/Login.vue'),
      meta: {
        libre: true
      }
    }
  ]
})
router.beforeEach( (to, from, next) => {
  if(to.matched.some(record => record.meta.libre)){
    next();
  }else if( store.state.usuario && store.state.usuario.rol === 'Administrador') {
    if(to.matched.some(record => record.meta.administrador)){
      next();
    }
  }else if( store.state.usuario && store.state.usuario.rol === 'Vendedor') {
    if(to.matched.some(record => record.meta.vendedor)){
      next();
    }
  }else if( store.state.usuario && store.state.usuario.rol === 'Almacenero') {
    if(to.matched.some(record => record.meta.almacenero)){
      next();
    }
  }else{
    next({name: 'login'})
  }
})
export default router;
