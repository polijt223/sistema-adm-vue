import axios from 'axios'
import store from '../store.js'

const endpoint = "usuario/"
let header = { "Token" : store.state.token};
let configuracion = {headers: header};

export const usuario = {
  async list() {
    try{  
      const listaHttp = await axios.get(`${endpoint}list`, configuracion);
      return listaHttp.data;
    }catch(error) {
        return error;
    }
  },
  async add(categoria) {
    try{
      let responseHttp = await axios.post(`${endpoint}add`, categoria, configuracion);
      return responseHttp.data
    }catch(error) {
      return error
    }
  },
  async put(categoria) {
    try{
      let responseHttp = await axios.put(`${endpoint}update`, categoria, configuracion);
      return responseHttp.data
    }catch(error) {
      return error
    }
  },
  async delete(paramId) {
    try{
      let responseHttp = await axios.delete(`${endpoint}remove`, { ...paramId , ...configuracion });
      return responseHttp.data
    }catch(error) {
      return error
    }
  },
  async getPerfil(paramId) {
    try{
      let responseHttp = await axios.get(`${endpoint}queryEmail`, { ...paramId , ...configuracion });
      return responseHttp.data
    }catch(error) {
      return error
    }
  },
  async activate(paramId) {
    try{
      let responseHttp = await axios.put(`${endpoint}activate`, paramId, configuracion)
      return responseHttp.data
    }catch(error) {
      return error
    }
  },
  async desactivate(paramId) {
    try{
      let responseHttp = await axios.put(`${endpoint}desactivate`, paramId, configuracion)
      return responseHttp.data
    }catch(error) {
      return error
    }
  }
}
