import axios from 'axios'
import store from '../store.js'

const endpoint = "venta/"
let header = { "Token" : store.state.token};
let configuracion = {headers: header};

export const venta = {
  async list() {
    try{  
      const listaHttp = await axios.get(`${endpoint}list`, configuracion);
      return listaHttp.data;
    }catch(error) {
        return error;
    }
  },
  async grafico12Meses() {
    try{  
      const listaHttp = await axios.get(`${endpoint}grafico12Meses`, configuracion);
      return listaHttp.data;
    }catch(error) {
        return error;
    }
  },
  async consultaFechas() {
    try{  
      const listaHttp = await axios.get(`${endpoint}consultaFechas`, configuracion);
      return listaHttp.data;
    }catch(error) {
        return error;
    }
  },
  async add(categoria) {
    try{
      let responseHttp = await axios.post(`${endpoint}add`, categoria, configuracion);
      return responseHttp.data
    }catch(error) {
      return error
    }
  },
  async activate(paramId) {
    try{
      let responseHttp = await axios.put(`${endpoint}activate`, paramId, configuracion)
      return responseHttp.data
    }catch(error) {
      return error
    }
  },
  async desactivate(paramId) {
    try{
      let responseHttp = await axios.put(`${endpoint}desactivate`, paramId, configuracion)
      return responseHttp.data
    }catch(error) {
      return error
    }
  }
}
